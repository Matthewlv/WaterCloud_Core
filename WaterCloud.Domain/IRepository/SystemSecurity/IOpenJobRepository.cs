﻿using System;
using System.Collections.Generic;
using System.Text;
using WaterCloud.DataBase;

namespace WaterCloud.Domain.SystemSecurity
{
    public interface IOpenJobRepository : IRepositoryBase<OpenJobEntity>
    {
    }
}
