﻿/*******************************************************************************
 * Copyright © 2020 WaterCloud.Framework 版权所有
 * Author: WaterCloud
 * Description: WaterCloud快速开发平台
 * Website：
*********************************************************************************/
using WaterCloud.DataBase;

namespace WaterCloud.Domain.SystemSecurity
{
    public interface ILogRepository : IRepositoryBase<LogEntity>
    {
        
    }
}
